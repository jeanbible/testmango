import {useState, useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Navigation from './components/Navigation'
import Publications from './components/Publications'
import ConnexionForm from './components/ConnexionForm'
import HomeMessage from './components/HomeMessage'
import { BrowserRouter as Router, Route} from 'react-router-dom'
import axios from 'axios';
function App() {
  const [publications, setPublications] = useState([])
  const [connected, setConnected] = useState(false)
  const api = axios.create({
    baseURL: `https://localhost:44327/api/publications/`

  })

  // function call after first render
  useEffect(() => {
    // get post list
    const getPublications = () => {
      api.get('/')
      .then(res =>{
        setPublications(res.data)
      })
      .catch(err => {
        console.log(err)
    })

    }
    getPublications()

    // verify local storage
    if(localStorage.getItem('token')){
      setConnected(true)
    }
  }, [])

  // delete post
  const deletePublication = async (id) =>{
    await api.delete(`/${id}`)
    setPublications(publications.filter((publication) => publication.id !== id));
  }

  // add post
  const addPublication = async (data) =>{
    await api.post(`/`,data )
    .then(res =>{
      setPublications([...publications, res.data])
    })
    .catch(err => {
      console.log(err)
  })
  }

  //updatePublication
  const modifPublication = async  (data) =>{
    await api.patch(`/${data.id}`,data )
    .then(res =>{
      setPublications(
        publications.map((publication) => publication.id === data.id ? {...publication, title: data.title, content: data.content } : publication)
      )
    })
    .catch(err => {
      console.log(err)
    })
  }
  return (
    <Router>
      <div className="app-container">
        <Navigation isConnected={connected} onDeconnexion={() => {setConnected(false)} } />
        <div className="container"> 
          <Route path='/' exact render={(props) => (
            <>{connected ? <HomeMessage /> :<ConnexionForm  connexion={() => {setConnected(true)} }/>}</> 
          )} />
          <Route path='/publications' exact render={(props) => (
              <Publications isConnected={connected} publications={publications} toDelete={deletePublication} toAdd={addPublication} toModif={modifPublication} />
          )} />
        </div>
      </div>
    </Router>
  );
}

export default App;
