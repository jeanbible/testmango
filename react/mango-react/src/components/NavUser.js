import {Link} from 'react-router-dom'
import {FaUserAlt} from 'react-icons/fa'
import Dropdown from 'react-bootstrap/Dropdown'
const NavUser = ({isConnected, onDeconnexion}) => {
    const willDeconnect = () =>{
        localStorage.clear()
        onDeconnexion()
    }
    return (
        <div>
            <Dropdown>
                <Dropdown.Toggle variant="primary" id="dropdown-basic">
                    <FaUserAlt />
                </Dropdown.Toggle>

                <Dropdown.Menu style={{left:"initial", right : 0}}>
                    <Link className="dropdown-item" to="/publications">Publication</Link>
                    <Link className="dropdown-item" to="/">{isConnected ? "Accueil" : 'Connexion'}</Link>
                    {isConnected && <span style={{cursor: "pointer"}} onClick={() => willDeconnect()} className="dropdown-item">Deconnexion</span>}
                    
                </Dropdown.Menu>
            </Dropdown>
        </div>
    )
}

export default NavUser
