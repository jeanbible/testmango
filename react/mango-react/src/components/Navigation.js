import Navbar from 'react-bootstrap/Navbar'
import logo from './../images/logo.svg';
import NavUser from './NavUser'
const Navigation = ({isConnected, onDeconnexion}) => {
    return (
<>
  <Navbar  bg="light" className="p-4" variant="dark">
    <Navbar.Brand href="/" className="mr-auto">
      <img
        alt=""
        src={logo}
        width="160"
        height="38"
        className="d-inline-block align-top"
      />{' '}
    </Navbar.Brand>
    <NavUser isConnected={isConnected} onDeconnexion={onDeconnexion} />
  </Navbar>
</>
    )
}

export default Navigation
