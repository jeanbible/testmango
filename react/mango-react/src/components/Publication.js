import Card from 'react-bootstrap/Card'
import DeleteModal from './DeleteModal'
import AddModifModal from "./AddModifModal"
const Publication = ({publication, isConnected, toDelete, toModif}) => {
    const publicationObj = {
        id: publication.id,
        title: publication.title,
        content: publication.content
    }
    return (
        <Card className="mt-3">
            <Card.Body>
                <Card.Title>{publication.title}</Card.Title>
                <Card.Text>{publication.content}</Card.Text>
                {isConnected && 
                <>
                    <AddModifModal type="modif" toModif={toModif} obj={publicationObj} />
                    <DeleteModal id={publication.id} toDelete={toDelete}/>
                </>
                }

            </Card.Body>
        </Card>
    )
}

export default Publication
