import Form from 'react-bootstrap/Form'
import {useState} from 'react'
import Button from 'react-bootstrap/Button'
import { Col, Row } from 'react-bootstrap'
import axios from 'axios';
import { useHistory } from "react-router-dom";

const ConnexionForm = ({connexion}) => {
    const history = useHistory()
    const [userName, setUserName] = useState('')
    const [password, setPassword] = useState('')
    const onSubmit = (e) => {
        e.preventDefault()

        // verification
        if(!userName){
            alert('Svp, remplir le champs identifiant')
            return
        }
        if(!password){
            alert('Svp, remplir le champs mot de passe')
            return
        }

        // build user object
        const data = {
            username: userName,
            password: password
        }

        // verify if user exist and set localStorage
        axios.post('https://localhost:44327/api/users/authenticate', data)
            .then(res => {
                localStorage.setItem('token', res.data.token)
                connexion()
                history.push('/publications')
            })
            .catch(err => {
                alert('L"identifiant ou le mot de passe n"existent pas')
            })
        }
        return (
            <Row className="justify-content-md-center">
                <Col xl={5}>
                    <Form className="mt-5 p-4 md-1  bg-light" onSubmit={onSubmit}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Identifiant</Form.Label>
                            <Form.Control value={userName} onChange={(e) => setUserName(e.target.value)} type="text" placeholder="Identifiant" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Mot de passe</Form.Label>
                            <Form.Control value={password} onChange={(e) => setPassword(e.target.value)} type="password" placeholder="Mot de passe" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Connexion
                        </Button>
                    </Form>
                </Col>
            </Row> 
    )
}

export default ConnexionForm
