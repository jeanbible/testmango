import {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'

const AddModifModal = ({type, toAdd, toModif, obj}) => {
    // state declaration
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const [show, setShow] = useState(false);

    //modal trigger
    const handleClose = () => setShow(false);
    const handleShow = () =>{
      if(type === "modif"){
        setContent(obj.content)
        setTitle(obj.title)
      }else{
        setContent("")
        setTitle("")
      }
      setShow(true);
    } 

    // info to construct publication object
    const submit = (e) =>{
      e.preventDefault()

      //form validation
      if(!title){
          alert('Svp, remplir le champs titre')
          return
      }
      if(!content){
          alert('Svp, remplir le champs description')
          return
      }

      if(type === "add"){
        addPublication()
      }else{
        modifPublication()
      }
      //close modal
      handleClose()
    }

    const addPublication = () =>{
      const data = {
        title: title,
        content: content
      }
      toAdd(data)
    }

    const modifPublication = () =>{
      const data = {
        id: obj.id,
        title: title,
        content: content
      }
      toModif(data)
    }

    return (
        <>
        <Button variant={ type === "add"  ? "success" : "primary" }  onClick={handleShow}>
          { type === "add"  ? "Ajouter une publication" : "Modifier"}  
        </Button>
  
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{ type === "add"  ? "Ajouter une publication" : "Modifier"}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form className="p-4 md-1  bg-light" onSubmit={submit} >
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Titre</Form.Label>
                    <Form.Control value={title} onChange={(e) => setTitle(e.target.value)} type="text" placeholder="Titre" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control value={content} onChange={(e) => setContent(e.target.value)} as="textarea" />
                </Form.Group>
                <Button variant={ type === "add"  ? "success" : "primary" } type="submit">
                  Sauvegarder
                </Button>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Fermer
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
}

export default AddModifModal
