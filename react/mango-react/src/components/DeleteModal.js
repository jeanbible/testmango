import {useState} from 'react'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
const DeleteModal = ({id, toDelete}) => {
    // boostrap modal handler
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const onDelete = () => {
      toDelete(id)
      handleClose()
    }
    return (
        <>
        <Button className="ml-2"  variant="danger" onClick={handleShow}>
          Supprimer
        </Button>
  
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Supprimer une publication</Modal.Title>
          </Modal.Header>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Fermer
            </Button>
            <Button variant="danger" onClick={() => onDelete()}>
              Confirmer la suppression
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
}

export default DeleteModal
