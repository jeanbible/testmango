import Publication from "./Publication"
import AddModifModal from "./AddModifModal"
const Publications = ({isConnected, publications, toDelete, toAdd, toModif}) => {
    return (
        <div className="publications-container mt-5">
            <h1 className="text-center">Liste de publications</h1>
            {isConnected 
                ? <AddModifModal type="add" toAdd={toAdd}/>
                : <p>Se connecter pour pouvoir ajouter ou modifier une publication</p>
            }
            {publications.length > 0 
                ? publications.map((publication, index) => (
                    <Publication key={index} publication={publication} isConnected={isConnected} toDelete={toDelete} toModif={toModif} />
                )) 
                : <p className="mt-3">Aucune Publications</p> }
        </div>
    )
}

export default Publications
