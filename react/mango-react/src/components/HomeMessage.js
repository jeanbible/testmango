const HomeMessage = () => {
    return (
        <div className="mt-4">
            <div className="row">
                <div className="col-xl-12">
                    <h1>Bienvenue sur votre gestionnaire de publications</h1>
                </div>
                <div className="col-xl-6">
                    <p>Vous êtes présentement connecté et pouvez maintenant modifier vos publications, utiliser la navigation pour vous déplacer.</p>
                </div>
            </div>
        </div>
    )
}

export default HomeMessage
