﻿using AutoMapper;
using MangoApi.Models;
using MangoApi.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoApi.Mapper
{
    public class MangoProjectMappings : Profile
    {
        public MangoProjectMappings()
        {
            CreateMap<Publication, PublicationDto>().ReverseMap();
        }
    }
}
