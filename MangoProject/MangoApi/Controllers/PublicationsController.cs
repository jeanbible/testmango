﻿using AutoMapper;
using MangoApi.Models;
using MangoApi.Models.Dtos;
using MangoApi.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PublicationsController : ControllerBase
    {
        private IPublicationRepository _pRepo;
        private readonly IMapper _mapper;

        public PublicationsController(IPublicationRepository pRepo, IMapper mapper)
        {
            _pRepo = pRepo;
            _mapper = mapper;
        }

        
        [HttpGet]
        //[Authorize]
        public IActionResult GetPublications()
        {
            var objList = _pRepo.GetPublications();
            var objDto = new List<PublicationDto>();
            foreach (var obj in objList)
            {
                objDto.Add(_mapper.Map<PublicationDto>(obj));
            }
            return Ok(objDto);
        }

        [HttpGet("{publicationId:int}", Name = "GetPublication")]
        //[Authorize]
        public IActionResult GetPublication(int publicationId)
        {
            var obj = _pRepo.GetPublication(publicationId);
            if (obj == null)
            {
                return NotFound();
            }
            var objDto = _mapper.Map<PublicationDto>(obj);
            return Ok(objDto);
        }

        [HttpPost]
        //[Authorize]
        public IActionResult CreatePublication([FromBody] PublicationDto publicationDto)
        {
            if (publicationDto == null)
            {
                return BadRequest(ModelState);
            }

            if (_pRepo.PublicationExists(publicationDto.Title))
            {
                ModelState.AddModelError("", "Publication Exists!");
                return StatusCode(404, ModelState);

            }

            var publicationObj = _mapper.Map<Publication>(publicationDto);

            if (!_pRepo.CreatePublication(publicationObj))
            {
                ModelState.AddModelError("", $"Something went wrong when saving the record {publicationObj.Title}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetPublication", new { publicationId = publicationObj.Id }, publicationObj);
        }

        [HttpDelete("{publicationId:int}", Name = "DeletePublication")]
        public IActionResult DeletePublication(int publicationId)
        {
            if (!_pRepo.PublicationExists(publicationId))
            {
                return NotFound();
            }

            var publicationObj = _pRepo.GetPublication(publicationId);

            if (!_pRepo.DeletePublication(publicationObj))
            {
                ModelState.AddModelError("", $"Something went wrong when deleting the record {publicationObj.Title}");
                return StatusCode(500, ModelState);
            }

            return NoContent();

        }

        [HttpPatch("{publicationId:int}", Name = "UpdatePublication")]
        public IActionResult UpdatePublication(int publicationId, [FromBody] PublicationDto publicationDto)
        {
            if (publicationDto == null || publicationId != publicationDto.Id)
            {
                return BadRequest(ModelState);
            }

            var publicationObj = _mapper.Map<Publication>(publicationDto);

            if (!_pRepo.UpdatePublication(publicationObj))
            {
                ModelState.AddModelError("", $"Something went wrong when updating the record {publicationObj.Title}");
                return StatusCode(500, ModelState);
            }

            return NoContent();

        }
    }
}
