﻿using MangoApi.Data;
using MangoApi.Models;
using MangoApi.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoApi.Repository
{
    public class PublicationRepository : IPublicationRepository
    {
        private readonly ApplicationDbContext _db;
        public PublicationRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public bool CreatePublication(Publication publication)
        {
            _db.Publications.Add(publication);
            return Save();
        }

        public bool DeletePublication(Publication publication)
        {
            _db.Publications.Remove(publication);
            return Save();
        }

        public Publication GetPublication(int publicationId)
        {
            return _db.Publications.FirstOrDefault(a => a.Id == publicationId);
        }

        public ICollection<Publication> GetPublications()
        {
            return _db.Publications.OrderBy(a => a.Title).ToList();
        }

        public bool PublicationExists(string name)
        {
            bool value = _db.Publications.Any(a => a.Title.ToLower().Trim() == name.ToLower().Trim());
            return value;
        }

        public bool PublicationExists(int id)
        {
            return _db.Publications.Any(a => a.Id == id);
        }

        public bool Save()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }

        public bool UpdatePublication(Publication publication)
        {
            _db.Publications.Update(publication);
            return Save();
        }
    }
}
