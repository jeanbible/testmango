﻿using MangoApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MangoApi.Repository.IRepository
{
    public interface IPublicationRepository
    {
        ICollection<Publication> GetPublications();
        Publication GetPublication(int publicationId);
        bool PublicationExists(string name);
        bool PublicationExists(int id);
        bool CreatePublication(Publication publication);
        bool UpdatePublication(Publication publication);
        bool DeletePublication(Publication publication);
        bool Save();
    }
}
